/* Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
Асинхронний JS та XML. Технологія, яка дозволяє завантажувати дані на веб-сторінці
без перезавантаження самої сторінки.
Використовується при отриманні даних, відправленні даних (наприклад форм, коментарів,
даних авторизації тощо)
*/

fetch("https://ajax.test-danit.com/api/swapi/films")
    .then((data) => data.json())
    .then((data) => {
        data.forEach(film => {
            createMovieCard(film, film.episodeId)
            getCharacters(film.characters)
                .then(characterList => setCharacters(characterList, film.episodeId))
        });
    })

function createMovieCard(film, id) {
    document.body.insertAdjacentHTML("beforebegin", `<div id=${id}><h2>EPISODE ${film.episodeId}</h2><h3>${film.name}</h3><p>${film.openingCrawl}</p></div>`)
}

function getCharacters(links) {
    return new Promise((resolve) => {
        const characters = []
        links.forEach(link => {
            fetch(link)
                .then(res => res.json())
                .then(character => {
                    characters.push(character.name)
                    if (characters.length === links.length) {
                        resolve(characters)
                    }
                })
        })
    })
}

function setCharacters(characterList, id) {
    const episode = document.getElementById(id)
    episode.insertAdjacentHTML("beforeend", `<ul>${characterList.map(item => {
        return `<li>${item}</li>`
    }).join("")}</ul>`)
}
